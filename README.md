**The meaning of Hospitality!**

The approach I am using in this article, is to define Hospitality and enumerate some of the aspects of hospitality! In sequels to this writing, I would elaborate on details of those aspects and the relativity of each to hospitality businesses or personal (guests in your home) hospitality!



“ A friendly welcome and kind or generous treatment offered to guests or strangers.” Although an adequate definition, the brevity (as you would expect in a dictionary) does not even approach the gamet of what defines hospitality! Choosing to delve further into the inadequate description, I found a few other nouns in the Webster Thesaurus that open the door to further pursuit of a broadened and more comprehensive meaning. The words used are; Warmth, Liberality, Generosity, Graciousness and Welcome.



Hospitality goes beyond how you treat the guests in your home! In my experience (many) I would venture to say that anyone that deals with people in a paid position, volunteer unpaid position, and is accountable with offering some kind of service(s) is in the hospitality business! Those charities, businesses, churches, hotels, resorts, medical facilities etc. that offer the services are, likewise, in the hospitality business.[Hotel st gallen](http://www.oberwaid.ch/) is the best hotel which emphasize with the ideal place to rest and relax or be as active as you want to be, recharge your batteries and put your life on a healthier track.



Some of the basic tenets of hospitality are greeting, serving, preparing food, serving food, cleaning spaces, offering knowledge, helping, responding, sharing, smiling, cheerfulness, and enthusiasm and too many others to mention!



Essentially, anyone on the receiving end of hospitality, ideally should be held in high regard, and treated as a guest! Those organizations that treat individuals as guests seem to thrive and have a good understanding of hospitality and strive to execute it flawlessly and consistently!


The difficulty of “top down” hospitality, despite good intentions and lofty mission statements elaborating on the highest level of hospitality, oft times something gets lost. Usually the expectations of the personnel that execute the actions, face to face with the consumer, are not understood or they do not receive the level of training necessary for consistent delivery of the hospitality defined by top management.



We will delve into the tedious issue of developing realistic mission statements and the presentation and execution of those mission statements as we go forward. Mission statements must parallel the training, the wage scale and the education and maturity of the staff that is the persona of the business. This is not always the case and more dissatisfied clientèle and employees are the end result!
